package it.epicode.be.es0804;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Rubrica rubrica = new Rubrica();

        try (Scanner scanner = new Scanner(System.in)) {

            int command = scanner.nextInt();

            switch (command) {
            case 1:
                rubrica.insert("Mario Rossi", "2344 43434 4545");
                break;
            }

        }
    }
}
