package it.epicode.be.es0804.th1;

public class Main {
    public static void main(String[] args) {
        SampleThread st1 = new SampleThread("ST_1");
        SampleThread st2 = new SampleThread("ST_2");

        st1.start();
        st2.start();
    }
}
